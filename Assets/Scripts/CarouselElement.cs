﻿using UnityEngine;
using System.Collections;

public class CarouselElement : MonoBehaviour {

	public Vector3 pivotPoint;
	public Texture elementTexture;
	public Renderer childRenderer;
	public int elementsCount  = 8;
	public int elementIndex = 0;

	void Start() {
		pivotPoint = transform.parent.position;
		Material mat = new Material(renderer.sharedMaterial);
		mat.SetTexture("_MainTex", elementTexture);
		renderer.sharedMaterial = mat;
		childRenderer.sharedMaterial = mat;
		transform.RotateAround(pivotPoint, Vector3.up, 360 * (elementIndex + 1) / elementsCount);
	}
}
