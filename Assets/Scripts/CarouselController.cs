﻿using UnityEngine;
using System.Collections;

public class CarouselController : MonoBehaviour {

	public CarouselElement[] elements;
	public float scrollingSpeed = 10.0f;
	public float dragScrollingSpeed = 0.04f;
	public Vector3 pivotPoint;
	private float defaultAngle = 0.0f;
	private Vector3 lastMousePosition;
	private float lastXTouchPosition = 0.0f;
	private Vector3 slowDownSpeed = new Vector3(0,0,0);
	private Quaternion rotateToQuaternion = Quaternion.identity;

	// Use this for initialization
	void Start () {
		pivotPoint = transform.position;
		defaultAngle = 360 / elements.Length;
		Input.simulateMouseWithTouches = true;
		lastMousePosition = Input.mousePosition;
		rotateToQuaternion = Quaternion.Euler(0.0f, 0.0f, 0.0f);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.touchCount > 0) {
			HandleTouch(Input.GetTouch(0));
		}
		if (Input.GetMouseButtonDown(0) ) {
			lastMousePosition = Input.mousePosition;
			slowDownSpeed.y = 0.0f;
		} else if (Input.GetMouseButton(0)) {
			float delta = lastMousePosition.x - Input.mousePosition.x;
			lastMousePosition = Input.mousePosition;
			if (delta != 0.0f) {
				SetSlowDownSpeed(delta);
			}
		}
		if (slowDownSpeed.y != 0.0f) {
			slowDownSpeed = Vector3.Lerp(slowDownSpeed, new Vector3(0,0,0), Time.smoothDeltaTime);
			transform.Rotate(slowDownSpeed);
		} else {
			transform.rotation = Quaternion.RotateTowards(transform.rotation, rotateToQuaternion, scrollingSpeed * Time.deltaTime);
		}
	}

	public void GoToNextElement() {
		SmoothRotation(defaultAngle);
	}

	public void GoToPreviousElement() {
		SmoothRotation(-defaultAngle);
	}

	public void OpenAdsURL() {
		Application.OpenURL("http://www.gog.com/thewitcher3/en");
	}

	public void OpenMaps() {
		Application.LoadLevel(1);
	}

	private void SetSlowDownSpeed(float angle) {
		slowDownSpeed.y = angle * dragScrollingSpeed;
	}

	private void SmoothRotation(float angle) {
		slowDownSpeed.y = 0.0f;
		float nextAngle = transform.rotation.eulerAngles.y + angle;
		float tmp = Mathf.Floor(nextAngle / defaultAngle);
		nextAngle = tmp * defaultAngle;
		rotateToQuaternion = Quaternion.Euler(0.0f, nextAngle, 0.0f);
	}

	private void HandleTouch(Touch touch) {
		if (touch.phase == TouchPhase.Began) {
			slowDownSpeed.y = 0.0f;
			lastXTouchPosition = touch.position.x;
		} else if (touch.phase == TouchPhase.Moved) {
			float delta = lastXTouchPosition - touch.position.x;
			SetSlowDownSpeed(delta);
		}
	}
}
