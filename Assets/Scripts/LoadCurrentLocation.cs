﻿using UnityEngine;
using System.Collections;

public class LoadCurrentLocation : MonoBehaviour {

	public GoogleMap mMapComponent = null;
	public int mSkipFrames = 100;
	public bool mAutoLocateCenter = false;
	public int mZoomSize = 13;

	private int mSkipedFrames = 0;

	// Use this for initialization
	void Start () {
		Input.location.Start(10.0f);
		if (mMapComponent == null) {
			Debug.LogError("MapComponent is null!");
			return;
		}
		mMapComponent.zoom = mZoomSize;
		mMapComponent.autoLocateCenter = mAutoLocateCenter;
	}
	
	// Update is called once per frame
	void Update () {
		if (skipFrame()) { // skip few frames before checking location again.
			return;
		}

		mSkipedFrames = 0;
		mMapComponent.enabled = true;
		if (!Input.location.isEnabledByUser) {
			Debug.LogWarning("location service is disabled by user in settings");
		}
		LocationInfo info = Input.location.lastData;
		GoogleMapLocation location = new GoogleMapLocation();
		if (Input.location.status == LocationServiceStatus.Failed) {
			Debug.LogWarning("location service failed");
			return;
		} else if (Input.location.status == LocationServiceStatus.Initializing) {
			Debug.LogWarning("location service Initializing");
			return;
		} else if (Input.location.status == LocationServiceStatus.Running) {
			Debug.Log("location service Running");
		} else {
			Debug.LogWarning("location service stopped");
		}
		Debug.Log("updating location: " + info.latitude + " , " + info.longitude);
		if (info.latitude == 0.0f && info.longitude == 0.0f) {
			location.latitude = 56.326368f;
			location.longitude = 44.003565f;
		} else {
			location.latitude = info.latitude;
			location.longitude = info.longitude;
		}
		location.address = "";
		mMapComponent.centerLocation = location;

		GoogleMapMarker[] markers = new GoogleMapMarker[2];
		markers[0] = new GoogleMapMarker();
		markers[0].color = GoogleMapColor.green;
		markers[0].label = "MyLocation";
		markers[0].size = GoogleMapMarker.GoogleMapMarkerSize.Small;
		markers[0].locations = new GoogleMapLocation[1];
		markers[0].locations[0] = location;

		markers[1] = new GoogleMapMarker();
		markers[1].color = GoogleMapColor.purple;
		markers[1].label = "MyLocation";
		markers[1].size = GoogleMapMarker.GoogleMapMarkerSize.Small;
		markers[1].locations = new GoogleMapLocation[1];
		markers[1].locations[0] = new GoogleMapLocation();
		markers[1].locations[0].address = "";
		markers[1].locations[0].latitude = location.latitude + 0.001f;
		markers[1].locations[0].longitude = location.longitude -0.005f;

		mMapComponent.markers = markers;

		mMapComponent.Refresh();
	}

	void OnDestroy() {
		Input.location.Stop();
	}

	private bool skipFrame() {
		return mSkipedFrames++ < mSkipFrames;
	}
}
